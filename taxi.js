if (Drupal.jsEnabled) {
  $(document).ready(function () {
		$("#edit-taxi-table-page-enable").click(
			function() {
				(this.checked) ?
					$("div.taxi_table_page_name").slideDown()
						:
					$("div.taxi_table_page_name").slideUp();
			});
		(taxi_table_page_enable == 1) ?
			$("div.taxi_table_page_name").slideDown()
				:
			$("div.taxi_table_page_name").slideUp();
	});
}
