Taxi (Ordered Taxonomy Display) Module
--------------------------------------

# Name

Ordered Taxonomy Display

# Version

0.5b for Drupal 5.x

# Objective

Show node vocabularies and terms in a ordered table, only in full node view.

# Usage

1. Install
To install, download and decompress in your modules directory (usually /sites/all/modules in Drupal 5.x).

2. Enable
Enable the module in Admin -> Site building -> Modules -> Other -> Ordered Taxonomy Display.

3. Configure
In the settings page (Admin -> Content Management -> Ordered Taxonomy Display) you can:
 * adjust the order in wich they will be shown in the table,
 * select which vocabularies are shown in the table, and
 * whether or not to show empty vocabularies.

# To-Do

	- Content type selection
	- Table caching, for performance improvements
	- Check Drupal 6 compatibility
	- Translation
